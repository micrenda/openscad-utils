use <involute_gear.scad>

gear_module = 10;
gear_tick = 5;
gear1_teeth = 23;
gear2_teeth =  15;


gear1_period = 1;
gear2_period = gear1_period * gear2_teeth / gear1_teeth;

d = gear_module * (gear1_teeth + gear2_teeth) / 2;

t = $t;
translate([0, 0]) 
    rotate(t / gear1_period * 360, [0,0,1])
        color("SeaGreen")
            linear_extrude(gear_tick)
                involute_gear(gear1_teeth, gear_module);
translate([d, 0])
    rotate(-t / gear2_period * 360, [0,0,1])
        rotate(gear2_teeth % 2 == 0 ? (360/gear2_teeth/2) : 0,[0,0,1])         
            color("Crimson")
                linear_extrude(gear_tick)
                    involute_gear(gear2_teeth, gear_module);