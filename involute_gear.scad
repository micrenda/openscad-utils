function reverse(list) = [for (i = [len(list)-1:-1:0]) list[i]];
function flatten(l) = [ for (a = l) for (b = a) b ] ;
function involute_curve(u, base_radius) = [
    base_radius * (cos(u * 180 / PI) + u * sin(u * 180 / PI)),
    base_radius * (sin(u * 180 / PI) - u * cos(u * 180 / PI))
    ];

/*!
  Create a 2D involute spur gear
  
  Arguments:
  teeth:        the number of teeth of the spur gear
  module_size:  the gear module defined as the reference diameter divided by the number of teeth
  pressure_angle: the gear pressure angle in degrees
  profile_shift:  the gear profile shift
  extra_dedendum_depth: move the dedendum lower (useful to avoid contacts)
  extra_dedendum_radius: increase the dedendum circle (useful to avoid contacts)


  This code is based on the otvinta spur gear calculator: http://www.otvinta.com/gear.html
*/
module involute_gear(teeth, module_size, pressure_angle=20, profile_shift=0, extra_dedendum_depth=0, extra_dedendum_radius=0) {
    ref_diameter = module_size * teeth;
	base_diameter = ref_diameter * cos(pressure_angle);
	base_radius = base_diameter / 2;
	tip_diameter = ref_diameter + 2 * module_size * (1 + profile_shift);
	tip_radius = tip_diameter / 2;
	tip_pressure_angle = acos(base_diameter / tip_diameter);
	inv_alpha = tan(pressure_angle) - pressure_angle * PI / 180;
	inv_alpha_a = tan(tip_pressure_angle) - tip_pressure_angle * PI / 180;
	top_thickness = PI / (2 * teeth) + (2 * profile_shift * tan(pressure_angle)) / teeth + inv_alpha - inv_alpha_a;
	top_thickness_degrees = top_thickness * 180 / PI;
	crest_width = tip_diameter * top_thickness;
    
    u_min = 0;		
	u_max = sqrt( tip_radius * tip_radius / base_radius / base_radius - 1);
    
    // Calculate angular tooth width along the base diameter.
    p1 = involute_curve(u_min, base_radius);
    p2 = involute_curve(u_max, base_radius);
	
	
	// distance between beginning and end of the involute curve
	d = sqrt( ( p1.x - p2.x ) * ( p1.x - p2.x ) + ( p1.y - p2.y ) * ( p1.y - p2.y));
	
 

	base_tooth_thickness = 2 * top_thickness_degrees + 2 * acos((base_radius * base_radius + tip_radius * tip_radius - d * d) / 2 / base_radius / tip_radius);

    u_div = $fn > 0 ? $fn : 360 / $fa;
    
	u_inc = (u_max - u_min) / u_div;
    
    pointsBase = [for(u =[u_min:u_inc:u_max]) 
    involute_curve(u, base_radius)];
    
    
    pointsA = [for (p = pointsBase) [p.x, p.y]];
    pointsB = [for (p = pointsBase) [p.x, -p.y]];
        
    omega = 360 / teeth;
    theta = (omega - base_tooth_thickness)/2;

    
    pointsA_rot = [for (p = pointsA) [p.x * cos(theta) - p.y * sin(theta), p.y * cos(theta) + p.x * sin(theta)]];
    
    pointsB_rot = [for (p = pointsB) [p.x * cos(-theta) - p.y * sin(-theta), p.y * cos(-theta) + p.x * sin(-theta)]];
    
    circle_center = [pointsA_rot[0].x - extra_dedendum_depth, 0];
    circle_radius = abs(pointsA_rot[0].y + extra_dedendum_radius);
    
    circle_angle_inc = $fn > 0 ? 360 / $fn : $fa;
    pointsC = [ for (a = [90:circle_angle_inc:270]) [ circle_center.x + circle_radius * cos(a), circle_center.y + circle_radius * sin(a)] ];
    
    
    points_section = concat(reverse(pointsA_rot), pointsC, pointsB_rot);

    points_combi = [ for (i = [0:teeth-1]) 
        [ for (p = points_section) 
            [
            p.x * cos(-i * omega) - p.y * sin(-i * omega),
            p.y * cos(-i * omega) + p.x * sin(-i * omega)    
            ]
        ]
    ];
        
    
        
    polygon(flatten(points_combi));

}



